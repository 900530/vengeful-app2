package lt.akademija;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

public class SastataView {
	
	private String notificationBar;

	private SastataModel sastataModel;
	
	public void create() {
		sastataModel.setRedaguojamasVagonas(new Sastata());
	}

	public void update(Integer id) {
		sastataModel.setRedaguojamasVagonas(sastataModel.searchSastataById(id));
	}

	public void delete(Integer id) {
		sastataModel.getVagonai().remove(sastataModel.searchSastataById(id));
		sastataModel.setRedaguojamasVagonas(null);
		zinute("Vagonas pasalintas");
	}

	public void cancel() {
		sastataModel.setRedaguojamasVagonas(null);
	}

	public void save() {
		Integer id = sastataModel.getRedaguojamasVagonas().getId();
		if (id != null) {
		} else {
			sastataModel.searchSastataById(id)
					.setNumeris(sastataModel.getRedaguojamasVagonas().getNumeris());
			sastataModel.searchSastataById(id)
					.setPagaminimoData(sastataModel.getRedaguojamasVagonas().getPagaminimoData());
			sastataModel.searchSastataById(id)
					.setPagaminusiImone(sastataModel.getRedaguojamasVagonas().getPagaminusiImone());
			sastataModel.searchSastataById(id)
					.setMiestas(sastataModel.getRedaguojamasVagonas().getMiestas());
			zinute("Vagonas atnaujintas");
			sastataModel.getRedaguojamasVagonas().setId(sastataModel.getVagonai().size());
			sastataModel.getVagonai().add(sastataModel.getRedaguojamasVagonas());
			zinute("Vagonas pridetas");
		}
		sastataModel.setRedaguojamasVagonas(null);
	}

	protected void zinute(String zinute) {
		FacesMessage doneMessage = new FacesMessage(zinute);
		FacesContext.getCurrentInstance().addMessage(null, doneMessage);
	}
	
	public SastataModel getSastataModel() {
		return sastataModel;
	}

	public void setSastataModel(SastataModel sastataModel) {
		this.sastataModel = sastataModel;
	}

	public String cPage(){
		return "success";
	}
	
	public String javaPage(){
		return "success";
	}
	
	public String testavimoPage(){
		return "success";
	}
	
	public String backMygtukas(){
		return "success";
	}
	public String getNotificationBar() {

		return notificationBar;
	}

	public void setNotificationBar(String notificationBar) {
		this.notificationBar = notificationBar;
	}

	
}
